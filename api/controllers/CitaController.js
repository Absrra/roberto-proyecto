/**
 * CitaController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: function(req,res) {
        Cita.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },
    update: function(req,res) {
        Cita.update({id: req.params.id}, req.body).exec((error, cita)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: cita
                })
            }
        })
    },
    destroy: function (req, res) {
        Cita.destroy({id: req.params.id}).exec(function (err, cita) {
            if(err){
                return res.status(500).send({
                    code: 500,
                    message: 'Error al eliminar datos',
                    content: paciente
                });
            }else{
                return res.status(200).send({
                    code: 200,
                    message: 'user delete',
                    content: cita
                });
            }
        });
    },
    find: function(req,res) {
        Cita.find().exec((error, ccita)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: ccita
                })
            }
        })
    },
    findOne: function(req,res) {
        Cita.findOne({id: req.params.id}).exec((error, cita)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: cita
                })
            }
        })
    }
  

};


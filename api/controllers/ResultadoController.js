/**
 * ResultadoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    create: function(req,res) {
        Resultado.create(req.body).exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    code: 500,
                    message: 'Error almacenando datos',
                    content: error
                });
            } else {
                return res.status(200).json({
                    code: 200,
                    message: 'Data almacenada exitosamente',
                    content: data
                });
            }
        })
    },
    update: function(req,res) {
        Resultado.update({id: req.params.id}, req.body).exec((error, resultado)  =>  {
            if (error) {
                return  res.status(500).send({
                    code:500,
                    message: 'Error al actualizar datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos actualizados correctamente',
                    content: resultado
                })
            }
        })
    },
    destroy: function (req, res) {
        Resultado.destroy({id: req.params.id}).exec(function (err, resultado) {
            if(err){
                return res.status(500).send({
                    code: 500,
                    message: 'Error al eliminar datos'
                });
            }else{
                return res.status(200).send({
                    code: 200,
                    message: 'user delete',
                    content: resultado
                });
            }
        });
    },
    find: function(req,res) {
        Resultado.find().populateAll().exec((error, cresultado)    =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: cresultado
                })
            }
        })
    },
    findOne: function(req,res) {
        Resultado.findOne({id: req.params.id}).populateAll().exec((error, resultado)  =>  {
            if (error) {
                return res.status(500).send({
                    code: 500,
                    message: 'Error al obtener datos',
                    content: error
                })
            } else {
                return res.status(200).send({
                    code: 200,
                    message: 'Datos obtenidos exitosamente',
                    content: resultado
                })
            }
        })
    }
  

};


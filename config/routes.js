/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  '/':                                'HomeController.index',

  /** Rutas paciente */
  'GET /paciente':                    'PacienteController.find',
  'POST /paciente':                   'PacienteController.create',
  'GET /paciente/:id':                'PacienteController.findOne',
  'PUT /paciente/:id':                'PacienteController.update',
  'DEL /paciente/:id':                'PacienteController.destroy',

  /** Rutas TipoExamen */
  'GET /examen':                      'TipoExamenController.find',
  'POST /examen':                     'TipoExamenController.create',
  'GET /examen/:id':                  'TipoExamenController.findOne',
  'PUT /examen/:id':                  'TipoExamenController.update',
  'DEL /examen/:id':                  'TipoExamenController.destroy',

  /** Rutas Resultado */
  'GET /resultado':                      'ResultadoController.find',
  'POST /resultado':                     'ResultadoController.create',
  'GET /resultado/:id':                  'ResultadoController.findOne',
  'PUT /resultado/:id':                  'ResultadoController.update',
  'DEL /resultado/:id':                  'ResultadoController.destroy',

  /** Rutas Resultado */
  'GET /historial':                      'HistorialController.find',
  'POST /historial':                     'HistorialController.create',
  'GET /historial/:id':                  'HistorialController.findOne',
  'PUT /historial/:id':                  'HistorialController.update',
  'DEL /historial/:id':                  'HistorialController.destroy',

  /** Rutas Resultado */
  'GET /cita':                           'CitaController.find',
  'POST /cita':                          'CitaController.create',
  'GET /cita/:id':                       'CitaController.findOne',
  'PUT /cita/:id':                       'CitaController.update',
  'DEL /cita/:id':                       'CitaController.destroy',

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝



  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
